﻿/*======================================
作者：洞庭夕照
创建：2017.8.21
网站：www.ninesky.cn
      mzwhj.cnblogs.com
代码：git.oschina.net/ninesky/Ninesky
版本：v1.0.0.0
======================================*/
using System;
using System.Collections.Generic;
using System.Text;
using Ninesky.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Ninesky.IService
{
    /// <summary>
    /// 栏目服务接口
    /// </summary>
    public interface ICategoryService:IBaseService<Category>
    {
        /// <summary>
        /// 查找子栏目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<IQueryable<Category>> FindChildrenAsync(int id);
    }
}
