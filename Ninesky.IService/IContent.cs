﻿/*======================================
作者：洞庭夕照
创建：2017.8.22
网站：www.ninesky.cn
      mzwhj.cnblogs.com
代码：git.oschina.net/ninesky/Ninesky
版本：v1.0.0.0
======================================*/
using System;
using System.Collections.Generic;
using System.Text;
using Ninesky.Models;

namespace Ninesky.IService
{
    /// <summary>
    /// 内容服务接口
    /// </summary>
    public interface IContent:IBaseService<Content>
    {
    }
}
