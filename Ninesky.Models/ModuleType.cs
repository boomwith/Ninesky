﻿/*======================================
作者：洞庭夕照
创建：2017.8.18
网站：www.ninesky.cn
      mzwhj.cnblogs.com
代码：git.oschina.net/ninesky/Ninesky
版本：v1.0.0.0
======================================
*/
using System.ComponentModel.DataAnnotations;

namespace Ninesky.Models
{
    /// <summary>
    /// 模块类型
    /// </summary>
    public enum ModuleType
    {
        [Display(Name = "未设置")]
        Null,
        /// <summary>
        /// 文章
        /// </summary>
        [Display(Name = "文章")]
        Article,
    }
}
