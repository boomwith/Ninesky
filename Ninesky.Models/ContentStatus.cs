﻿/*======================================
作者：洞庭夕照
创建：2017.8.22
网站：www.ninesky.cn
      mzwhj.cnblogs.com
代码：git.oschina.net/ninesky/Ninesky
版本：v1.0.0.0
======================================*/

using System.ComponentModel.DataAnnotations;

namespace Ninesky.Models
{
    /// <summary>
    /// 内容状态
    /// </summary>
    public enum ContentStatus
    {
        /// <summary>
        /// 草稿
        /// </summary>
        [Display(Name = "草稿")]
        Draft,
        /// <summary>
        /// 退稿
        /// </summary>
        [Display(Name = "退稿")]
        Rejection,
        /// <summary>
        /// 删除
        /// </summary>
        [Display(Name = "已删除")]
        Deleted,
        /// <summary>
        /// 未审核
        /// </summary>
        [Display(Name = "未审核")]
        Revised,
        /// <summary>
        /// 正常
        /// </summary>
        [Display(Name = "正常")]
        Normal = 99
    }
}
