﻿/*======================================
作者：洞庭夕照
创建：2017.8.21
网站：www.ninesky.cn
      mzwhj.cnblogs.com
代码：git.oschina.net/ninesky/Ninesky
版本：v1.0.0.0
======================================*/
using System;
using System.Collections.Generic;
using System.Text;
using Ninesky.Models;
using Ninesky.IService;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;

namespace Ninesky.Service
{
    /// <summary>
    /// 栏目服务
    /// </summary>
    public class CategoryService:BaseService<Category>,ICategoryService
    {
        public CategoryService(DbContext dbContext):base(dbContext)
        {
            
        }

        public override OperationResult Add(Category entity, bool isSave = true)
        {
            //设置默认值
            entity.ChildNum = 0;
            OperationResult opsResult = new OperationResult() { Succeed = true };
            //检查父栏目
            if (entity.ParentId > 0)
            {
                var parentCategory = Find(entity.ParentId);
                if (parentCategory == null)
                {
                    opsResult.Succeed = false;
                    opsResult.Message = "父栏目不存在";
                }
                else if (parentCategory.Type != CategoryType.General)
                {
                    opsResult.Succeed = false;
                    opsResult.Message = "父栏目不是常规栏目";
                }
                else
                {
                    entity.ParentPath = parentCategory.ParentPath + "," + parentCategory.CategoryId;
                    entity.Depth = parentCategory.Depth + 1;
                    parentCategory.ChildNum++;
                    Update(parentCategory, false);
                }
            }
            //父栏目为0
            else
            {
                entity.ParentPath = "0";
                entity.Depth = 0;
            }
            if (opsResult.Succeed) opsResult = base.Add(entity, isSave);
            return opsResult;
        }

        public override async Task<OperationResult> AddAsync(Category entity, bool isSave = true)
        {
            //设置默认值
            entity.ChildNum = 0;
            OperationResult opsResult = new OperationResult() { Succeed = true };
            //检查父栏目
            if (entity.ParentId > 0)
            {
                var parentCategory = await FindAsync(entity.ParentId);
                if (parentCategory == null)
                {
                    opsResult.Succeed = false;
                    opsResult.Message = "父栏目不存在";
                }
                else if (parentCategory.Type != CategoryType.General)
                {
                    opsResult.Succeed = false;
                    opsResult.Message = "父栏目不是常规栏目";
                }
                else
                {
                    entity.ParentPath = parentCategory.ParentPath + "," + parentCategory.CategoryId;
                    entity.Depth = parentCategory.Depth + 1;
                    parentCategory.ChildNum++;
                    await UpdateAsync(parentCategory, false);
                }
            }
            //父栏目为0
            else
            {
                entity.ParentPath = "0";
                entity.Depth = 0;
            }
            if (opsResult.Succeed) opsResult = await base.AddAsync(entity, isSave);
            return opsResult;
        }

        public async Task<IQueryable<Category>> FindChildrenAsync(int id)
        {
            return await FindListAsync<int>(0, c => c.ParentId == id, c => c.Order, true);
        }

        public override async Task<OperationResult> RemoveAsync(Category entity, bool isSave = true)
        {
            OperationResult opsResult = new OperationResult();
            Category category = await FindAsync(entity.CategoryId);
            if (category == null) return new OperationResult() { Succeed = false, Message = "栏目不存在" };
            if (category.Type == CategoryType.General)
            {
                if (category.ChildNum > 0) return new OperationResult() { Succeed = false, Message = "请先删除子栏目" };
                //此处应检查是否有内容
            }
            int parentId = category.ParentId;
            //父栏目中的子栏目数目减1
            if (parentId > 0)
            {
                var parent = await FindAsync(parentId);
                if (parent != null)
                {
                    parent.ChildNum--;
                    await UpdateAsync(parent, false);
                }
            }
            opsResult = await base.RemoveAsync(entity, isSave);
            return opsResult;
        }
    }
}
