﻿/*======================================
作者：洞庭夕照
创建：2017.8.18
网站：www.ninesky.cn
      mzwhj.cnblogs.com
代码：git.oschina.net/ninesky/Ninesky
版本：v1.0.0.0
======================================
*/
using System;
using Ninesky.IService;
using System.Threading.Tasks;
using Ninesky.Models;
using System.Linq.Expressions;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Ninesky.Service
{
    /// <summary>
    /// 服务基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseService<T>:IBaseService<T> where T:class
    {
        private DbContext _dbContext;
        public DbContext DbContext { get { return _dbContext; } }

        public BaseService(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="isSave">是否立即保存</param>
        /// <returns>添加的记录数[isSave=true时有效]</returns>
        public virtual OperationResult Add(T entity, bool isSave = true)
        {
            OperationResult opsResult = new OperationResult();
            _dbContext.Set<T>().Add(entity);
            if (isSave)
            {
                opsResult.Succeed = _dbContext.SaveChanges() > 0;
                opsResult.Message = opsResult.Succeed ? "添加成功" : "添加失败";
            }
            else
            {
                opsResult.Succeed = false;
                opsResult.Message = "添加已缓存";
            }
            return opsResult;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="isSave">是否立即保存</param>
        /// <returns>添加的记录数[isSave=true时有效]</returns>
        public virtual async Task<OperationResult> AddAsync(T entity, bool isSave = true)
        {
            OperationResult opsResult = new OperationResult();
            await _dbContext.Set<T>().AddAsync(entity);
            if (isSave)
            {
                opsResult.Succeed = _dbContext.SaveChanges() > 0;
                opsResult.Message = opsResult.Succeed ? "添加成功" : "添加失败";
            }
            else
            {
                opsResult.Succeed = false;
                opsResult.Message = "添加已缓存";
            }
            return opsResult;
        }

        /// <summary>
        /// 查询记录数
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <returns>记录数</returns>
        public virtual int Count(Expression<Func<T, bool>> predicate)
        {
            return _dbContext.Set<T>().Count(predicate);
        }

        /// <summary>
        /// 查询记录数
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <returns>记录数</returns>
        public virtual async Task<int> CountAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbContext.Set<T>().CountAsync(predicate);
        }

        /// <summary>
        /// 查询是否存在
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <returns>是否存在</returns>
        public virtual bool Exists(Expression<Func<T, bool>> predicate)
        {
            return _dbContext.Set<T>().Any(predicate);
        }

        /// <summary>
        /// 查询是否存在
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <returns>是否存在</returns>
        public virtual async Task<bool> ExistsAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbContext.Set<T>().AnyAsync(predicate);
        }

        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="keyValues">主键</param>
        /// <returns></returns>
        public virtual T Find(params object[] keyValues)
        {
            return _dbContext.Set<T>().Find(keyValues);
        }

        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="keyValues">主键</param>
        /// <returns></returns>
        public virtual async Task<T> FindAsync(params object[] keyValues)
        {
            return await _dbContext.Set<T>().FindAsync(keyValues);
        }

        public virtual T Find(Expression<Func<T, bool>> predicate)
        {
            return _dbContext.Set<T>().SingleOrDefault(predicate);
        }

        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <returns></returns>
        public virtual async Task<T> FindAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbContext.Set<T>().SingleOrDefaultAsync(predicate);
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<T> FindAll()
        {
            return _dbContext.Set<T>();
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <returns>实体列表</returns>
        public virtual async Task<IQueryable<T>> FindAllAsync()
        {
            IQueryable<T> result = _dbContext.Set<T>();
            return await Task.FromResult(result);
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="number">显示数量[小于等于0-不启用]</param>
        /// <typeparam name="TKey">排序字段</typeparam>
        /// <param name="predicate">查询条件</param>
        /// <param name="keySelector">排序</param>
        /// <param name="isAsc">正序</param>
        /// <returns></returns>
        public virtual IQueryable<T> FindList<TKey>(int number, Expression<Func<T, bool>> predicate, Expression<Func<T, TKey>> keySelector, bool isAsc)
        {
            var entityList = _dbContext.Set<T>().Where(predicate);
            if (isAsc) entityList = entityList.OrderBy(keySelector);
            else entityList.OrderByDescending(keySelector);
            if (number > 0) return entityList.Take(number);
            else return entityList;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="number">显示数量[小于等于0-不启用]</param>
        /// <typeparam name="TKey">排序字段</typeparam>
        /// <param name="predicate">查询条件</param>
        /// <param name="keySelector">排序</param>
        /// <param name="isAsc">正序</param>
        /// <returns></returns>
        public virtual async Task<IQueryable<T>> FindListAsync<TKey>(int number, Expression<Func<T, bool>> predicate, Expression<Func<T, TKey>> keySelector, bool isAsc)
        {
            var entityList = _dbContext.Set<T>().Where(predicate);
            if (isAsc) entityList = entityList.OrderBy(keySelector);
            else entityList.OrderByDescending(keySelector);
            if (number > 0) entityList = entityList.Take(number);
            return await Task.FromResult(entityList);
        }

        /// <summary>
        /// 查询[分页]
        /// </summary>
        /// <typeparam name="TKey">排序属性</typeparam>
        /// <param name="predicate">查询条件</param>
        /// <param name="keySelector">排序</param>
        /// <param name="isAsc">是否正序</param>
        /// <param name="paging">分页数据</param>
        /// <returns></returns>
        public virtual Paging<T> FindPaging<TKey>(Expression<Func<T, bool>> predicate, Expression<Func<T, TKey>> keySelector, bool isAsc, Paging<T> paging)
        {
            var entityList = _dbContext.Set<T>().Where(predicate);
            paging.Total = entityList.Count();
            if (isAsc) entityList = entityList.OrderBy(keySelector);
            else entityList.OrderByDescending(keySelector);
            paging.Entities = entityList.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToList();
            return paging;
        }

        /// <summary>
        /// 查询[分页]
        /// </summary>
        /// <typeparam name="TKey">排序属性</typeparam>
        /// <param name="predicate">查询条件</param>
        /// <param name="keySelector">排序</param>
        /// <param name="isAsc">是否正序</param>
        /// <param name="paging">分页数据</param>
        /// <returns></returns>
        public virtual async Task<Paging<T>> FindPagingAsync<TKey>(Expression<Func<T, bool>> predicate, Expression<Func<T, TKey>> keySelector, bool isAsc, Paging<T> paging)
        {
            var entityList = _dbContext.Set<T>().Where(predicate);
            paging.Total = await entityList.CountAsync();
            if (isAsc) entityList = entityList.OrderBy(keySelector);
            else entityList.OrderByDescending(keySelector);
            paging.Entities = await entityList.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToListAsync();
            return paging;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="isSave">是否立即保存</param>
        /// <returns>是否删除成功[isSave=true时有效]</returns>
        public virtual OperationResult Remove(T entity, bool isSave = true)
        {
            OperationResult opsResult = new OperationResult();
            _dbContext.Set<T>().Remove(entity);
            if (isSave) opsResult.Succeed = _dbContext.SaveChanges() > 0;
            else opsResult.Succeed = false;
            return opsResult;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="isSave">是否立即保存</param>
        /// <returns>是否删除成功[isSave=true时有效]</returns>
        public virtual async Task<OperationResult> RemoveAsync(T entity, bool isSave = true)
        {
            OperationResult opsResult = new OperationResult();
            _dbContext.Set<T>().Remove(entity);
            if (isSave) opsResult.Succeed = await _dbContext.SaveChangesAsync() > 0;
            else opsResult.Succeed = false;
            return opsResult;
        }


        /// <summary>
        ///  保存数据
        /// </summary>
        /// <returns>更改的记录数</returns>
        public virtual int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        /// <summary>
        ///  保存数据
        /// </summary>
        /// <returns>更改的记录数</returns>
        public virtual async Task<int> SaveChangesAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="isSave">是否立即保存</param>
        /// <returns>是否保存成功</returns>
        public virtual bool Update(T entity, bool isSave = true)
        {
            _dbContext.Set<T>().Update(entity);
            if (isSave) return _dbContext.SaveChanges() > 0;
            else return false;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="isSave">是否立即保存</param>
        /// <returns>[isSave=true时有效]\n
        /// OperationResult.Succeed操作是否成功。
        /// </returns>
        public virtual async Task<OperationResult> UpdateAsync(T entity, bool isSave = true)
        {
            var oResult = new OperationResult();
            _dbContext.Set<T>().Update(entity);
            if (isSave) oResult.Succeed = await _dbContext.SaveChangesAsync() > 0;
            return oResult;
        }
    }
}
